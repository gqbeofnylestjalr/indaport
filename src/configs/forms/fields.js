import React from 'react';

import validations from './validations';

const fields = {
  title: (Component = ({ title }) => <h4> {title} </h4>) => ({
    name: 'title',
    Component: ({ text }) => <Component title={text} />
  }),
  name: {
    name: 'name',
    type: 'text',
    validation: validations.nonEmpty,
    i18n: { placeHolder: '', label: 'name' }
  },
  email: {
    name: 'email',
    type: 'text',
    validation: validations.email,
    i18n: { placeHolder: 'user@gmail.com', label: 'email' }
  },
  login: {
    name: 'login',
    type: 'text',
    validation: validations.login,
    i18n: { placeHolder: 'username', label: 'username' }
  },
  phoneNumber: {
    name: 'phoneNumber',
    type: 'text',
    validation: validations.phoneNumber,
    i18n: { placeHolder: 'xxxxxxxxxxxx', label: 'phone' }
  },
  password: {
    name: 'password',
    type: 'password',
    validation: validations.password,
    i18n: { placeHolder: '*******************', label: 'password' }
  },
  oldPassword: {
    name: 'old_password',
    type: 'password',
    validation: validations.password,
    i18n: { placeHolder: '*******************', label: 'old password' }
  },
  newPassword: {
    name: 'new_password',
    type: 'password',
    compareTo: 'confirmPassword',
    validation: validations.samePassword,
    i18n: { placeHolder: '*******************', label: 'new password' }
  },
  confirmPassword: {
    name: 'confirmPassword',
    type: 'password',
    compareWith: 'new_password',
    validation: validations.samePassword,
    i18n: {
      placeHolder: '*******************',
      label: 'confirm password'
    }
  },
  apiKey: {
    name: 'apiKey',
    type: 'text',
    validation: validations.nonEmpty,
    i18n: { placeHolder: '', label: 'api key' }
  },
  apiSecret: {
    name: 'apiSecret',
    type: 'text',
    validation: validations.nonEmpty,
    i18n: { placeHolder: '', label: 'api secret' }
  }
};

export default fields;
