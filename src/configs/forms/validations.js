const validations = {
  nothing: () => ({
    valid: true
  }),
  login: ({ field, form }) => ({
    valid: /^(?=.*[A-Za-z])[A-Za-z0-9!@#/,<>?":$%^&*)(+=._-]{0,}$/.test(
      form[field].value
    )
  }),
  nonEmpty: ({ field, form }) => ({
    valid: /^(?!\s*$).+/.test(form[field].value)
  }),
  digits: ({ field, form }) => ({
    valid: /^[+-]?\d+(\.\d+)?$/.test(form[field].value)
  }),
  phoneNumber: ({ field, form }) => ({
    valid: /^[+]?\d{11}$/.test(form[field].value)
  }),
  email: ({ field, form }) => ({
    valid: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(form[field].value)
  }),
  password: ({ field, form }) => ({
    valid: /^(?=.*[A-Za-z])[A-Za-z0-9!@#/,<>?":$%^&*)(+=._-]{6,}$/.test(
      form[field].value
    )
  }),
  samePassword: ({ field, form }) => {
    const { compareTo, compareWith } = form[field];

    return compareTo
      ? [
          {
            field,
            result: {
              valid: /^(?=.*[A-Za-z])[A-Za-z0-9!@#/,<>?":$%^&*)(+=._-]{6,}$/.test(
                form[field].value
              )
            }
          },
          {
            field: [compareTo],
            result: {
              valid:
                /^(?=.*[A-Za-z])[A-Za-z0-9!@#/,<>?":$%^&*)(+=._-]{6,}$/.test(
                  form[field].value
                ) && form[field].value === form[compareTo].value
            }
          }
        ]
      : {
          valid:
            /^(?=.*[A-Za-z])[A-Za-z0-9!@#/,<>?":$%^&*)(+=._-]{6,}$/.test(
              form[field].value
            ) && form[field].value === form[compareWith].value
        };
  },
  refillGive: ({ field, form, currency }) => ({
    valid:
      /^[+-]?\d+(\.\d+)?$/.test(form[field].value) &&
      form[field].value >= currency.fee.minSum &&
      form[field].value <= currency.fee.maxSum
  })
};

export default validations;
