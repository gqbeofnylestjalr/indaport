import React from 'react';
import { Link } from 'react-router-dom';

import validations from './validations';
import fields from './fields';
import ajaxHandlers from './ajaxHandlers';

const formConnectApiConfig = name => ({
  status: 'awaiting',
  method: '/setpartneruserapikey',
  i18n: { submit: 'save changes', title: 'connect to exchange' },
  fields: [
    {
      name: 'logo',
      Component: ({ imgSrc }) => <img src={imgSrc} alt={name} />
    },
    { ...fields.title() },
    {
      name: 'market',
      type: 'text',
      value: name,
      readOnly: true,
      valid: true,
      validation: validations.nonEmpty,
      i18n: { placeHolder: '', label: 'name' }
    },
    { ...fields.apiKey },
    {
      name: 'howTo',
      Component: ({ t }) => (
        <p>
          <Link to="/how-to-create-api-key">{t('how to create api key')}?</Link>
        </p>
      )
    },
    { ...fields.apiSecret }
  ],
  handler: ajaxHandlers.connectApiHandler
});

const formConfigs = {
  login: {
    status: 'awaiting',
    method: '/checkpartneruser',
    i18n: { submit: 'login', title: 'sign in to indaport' },
    fields: [
      { ...fields.title() },
      { ...fields.email },
      { ...fields.password }
    ],
    handler: ajaxHandlers.loginHandler
  },
  registration: {
    status: 'awaiting',
    method: '/createpartneruser',
    i18n: {
      submit: 'create account',
      title: 'create your account',
      sending: 'sending'
    },
    fields: [
      { ...fields.title() },
      { ...fields.email },
      { ...fields.phoneNumber, optional: true }
    ],
    handler: ajaxHandlers.defaultHandler
  },
  passwordRecovery: {
    status: 'awaiting',
    method: '/resetpasswordpartneruser',
    i18n: {
      submit: 'reset password',
      title: 'password recovery',
      sending: 'sending'
    },
    fields: [{ ...fields.title() }, { ...fields.email }],
    handler: ajaxHandlers.defaultHandler
  },
  confirmation: {
    status: 'awaiting',
    method: '/setpasswordandconfirmpartneruser',
    i18n: {
      submit: 'confirm',
      title: 'confirm your account and set the password',
      sending: 'sending'
    },
    fields: [
      { ...fields.title() },
      {
        name: 'email',
        type: 'text',
        readOnly: true,
        validation: validations.email,
        i18n: { placeHolder: 'email', label: 'email' }
      },
      { ...fields.password },
      {
        name: 'confirmation_hash',
        type: 'text',
        readOnly: true,
        validation: validations.nonEmpty,
        i18n: { placeHolder: 'confirmation hash', label: 'confirmation hash' }
      }
    ],
    handler: ajaxHandlers.defaultHandler
  },
  settings: {
    personalData: {
      status: 'awaiting',
      i18n: { submit: 'save changes', title: 'personal data' },
      fields: [
        { ...fields.title(({ title }) => <h3> {title} </h3>) },
        { ...fields.email, optional: true },
        {
          name: 'name',
          optional: true,
          type: 'text',
          validation: validations.nothing,
          i18n: { placeHolder: 'name', label: 'name' }
        },
        {
          name: 'surname',
          optional: true,
          type: 'text',
          validation: validations.nothing,
          i18n: { placeHolder: 'surname', label: 'surname' }
        },
        { ...fields.phoneNumber, optional: true }
      ],
      handler: ajaxHandlers.defaultHandler
    },
    changePassword: {
      status: 'awaiting',
      method: '/changepasswordpartneruser',
      i18n: { submit: 'save changes', title: 'change password' },
      fields: [
        { ...fields.title(({ title }) => <h3> {title} </h3>) },
        { ...fields.oldPassword },
        { ...fields.newPassword },
        { ...fields.confirmPassword }
      ],
      handler: ajaxHandlers.changePassword
    }
  },
  connectApi: {
    binance: formConnectApiConfig('binance'),
    bittrex: formConnectApiConfig('bittrex'),
    hitbtc: formConnectApiConfig('hitbtc')
  },
  refill: {
    status: 'awaiting',
    isSubmiting: true,
    i18n: { submit: 'refill' },
    fields: [
      {
        name: 'market',
        value: 'binance',
        valid: true,
        validation: validations.nonEmpty
      },
      {
        name: 'give',
        currency: 'USD',
        validation: validations.refillGive
      },
      {
        name: 'get',
        currency: 'BTC',
        validation: validations.digits
      },
      {
        name: 'walletAddress',
        validation: validations.nonEmpty,
        tag: ''
      }
    ],
    handler: ajaxHandlers.defaultHandler
  },
  refillMoment: {
    status: 'awaiting',
    isSubmiting: true,
    i18n: { submit: 'refill' },
    fields: [
      {
        name: 'give',
        currency: 'USD',
        validation: validations.refillGive
      },
      {
        name: 'get',
        currency: 'BTC',
        validation: validations.digits
      },
      {
        name: 'walletAddress',
        validation: validations.nonEmpty,
        tag: ''
      }
    ],
    handler: ajaxHandlers.defaultHandler
  }
};

export default formConfigs;
