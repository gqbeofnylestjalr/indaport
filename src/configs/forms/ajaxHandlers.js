const ajaxHandlers = {
  defaultHandler: ({
    response,
    formPath,
    setStatus,
    dispatch,
    clearForm,
    messageDuration
  }) => {
    dispatch(setStatus(formPath, response.data.status));
    setTimeout(
      () => dispatch(setStatus(formPath, 'awaiting')),
      messageDuration
    );
  },
  loginHandler: ({
    response,
    login,
    formPath,
    setStatus,
    dispatch,
    clearForm,
    setLoadStatus,
    getCurrenciesLists,
    messageDuration
  }) => {
    if (response.data.status === true) {
      dispatch(clearForm(formPath));
      dispatch(setLoadStatus(false));
      dispatch(login(response.data.userData));
      dispatch(getCurrenciesLists()).then(() => {
        dispatch(setLoadStatus(true));
      });
    } else {
      dispatch(setStatus(formPath, 'unsuccessful login'));
      setTimeout(
        () => dispatch(setStatus(formPath, 'awaiting')),
        messageDuration
      );
    }
  },
  connectApiHandler: ({
    response,
    isLogined,
    formPath,
    setStatus,
    dispatch,
    clearForm,
    messageDuration
  }) => {
    dispatch(setStatus(formPath, response.data.status));
    dispatch(isLogined());
    setTimeout(() => {
      dispatch(setStatus(formPath, 'awaiting'));
    }, messageDuration);
  },
  changePassword: ({
    response,
    formPath,
    setStatus,
    dispatch,
    clearForm,
    messageDuration
  }) => {
    dispatch(setStatus(formPath, response.data.status));
    setTimeout(() => {
      if (response.data.status === 'password change confirm') {
        dispatch(clearForm(formPath));
      }
      dispatch(setStatus(formPath, 'awaiting'));
    }, messageDuration);
  }
};

export default ajaxHandlers;
