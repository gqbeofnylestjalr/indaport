import React from 'react';
import { Switch } from 'react-router-dom';

import Landing from '../components/Landing';

import Login from '../components/Login';
import Registration from '../components/Registration';
import Confirmation from '../components/Confirmation';
import PasswordRecovery from '../components/PasswordRecovery';

import ConnectApi, { ConnectApiForm } from '../components/ConnectApi';

import Partners from '../components/Partners';
import Settings from '../components/Settings';

import Refill, { RefillMoment } from '../components/Refill';

import ConditionRoute from '../HOC/ConditionRoute';

const Routes = props => {
  const {
    t,
    user,
    ui,
    toggleDropdown,
    editInput,
    getValueFromPlaceholder,
    changeRefillInput,
    changeCurrency,
    submitForm,
    forms,
    currencies,
    location
  } = props;

  const { dropdowns } = ui;

  const formProps = { t, editInput, submitForm };

  const refillProps = {
    t,
    user,
    editInput,
    getValueFromPlaceholder,
    changeRefillInput,
    changeCurrency,
    toggleDropdown,
    dropdowns,
    currencies,
    location
  };

  const isLogined = user.logined;
  return (
    <Switch>
      <ConditionRoute
        exact
        condition={!isLogined}
        route="/refill-moment"
        path="/"
        render={() => <Landing {...props} />}
      />
      <ConditionRoute
        exact
        condition={!isLogined}
        route="/refill-moment"
        path="/registration"
        render={() => (
          <Registration
            {...formProps}
            form={forms.registration}
            location={location}
            user={user}
          />
        )}
      />
      <ConditionRoute
        path="/confirmation"
        condition={!isLogined}
        route="/refill-moment"
        render={() => (
          <Confirmation
            {...formProps}
            form={forms.confirmation}
            location={location}
            user={user}
          />
        )}
      />
      <ConditionRoute
        path="/login"
        condition={!isLogined}
        route="/refill-moment"
        render={() => <Login {...formProps} form={forms.login} user={user} />}
      />

      <ConditionRoute
        path="/password-recovery"
        condition={!isLogined}
        route="/refill-moment"
        render={() => (
          <PasswordRecovery {...formProps} form={forms.passwordRecovery} />
        )}
      />

      <ConditionRoute
        exact
        condition={isLogined}
        path="/connect-api"
        render={() => <ConnectApi t={t} user={user} />}
      />

      <ConditionRoute
        path="/connect-api/binance"
        condition={isLogined}
        render={() => (
          <ConnectApiForm
            {...formProps}
            user={user}
            form={forms.connectApi.binance}
            imgSrc="/img/binance.png"
          />
        )}
      />

      <ConditionRoute
        path="/connect-api/bittrex"
        condition={isLogined}
        render={() => (
          <ConnectApiForm
            {...formProps}
            user={user}
            form={forms.connectApi.bittrex}
            imgSrc="/img/bittrex.svg"
          />
        )}
      />

      <ConditionRoute
        path="/connect-api/hitbtc"
        condition={isLogined}
        render={() => (
          <ConnectApiForm
            {...formProps}
            user={user}
            form={forms.connectApi.hitbtc}
            imgSrc="/img/hitbtc.svg"
          />
        )}
      />

      <ConditionRoute
        path="/partners"
        condition={isLogined}
        render={() => <Partners t={t} />}
      />

      <ConditionRoute
        path="/settings"
        condition={isLogined}
        render={props => (
          <Settings
            {...formProps}
            {...props}
            personalDataForm={forms.settings.personalData}
            changePasswordForm={forms.settings.changePassword}
          />
        )}
      />

      <ConditionRoute
        path="/refill"
        condition={isLogined}
        render={() => <Refill form={forms.refill} {...refillProps} />}
      />

      <ConditionRoute
        path="/refill-moment"
        condition={isLogined}
        render={() => (
          <RefillMoment form={forms.refillMoment} {...refillProps} />
        )}
      />
    </Switch>
  );
};

export default Routes;
