import React from 'react';

import Form from '../Form';

const Settings = ({ personalDataForm, changePasswordForm, ...rest }) => (
  <div className="page-partner page-settings page-wrap">
    <div className="container">
      <div className="row mt-5">
        <div className="col-12 col-md-6 mb-4">
          <div className="partner-block">
            <Form {...rest} form={personalDataForm} />
          </div>
        </div>
        <div className="col-12 col-md-6 mb-4">
          <div className="partner-block">
            <Form {...rest} form={changePasswordForm} />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Settings;
