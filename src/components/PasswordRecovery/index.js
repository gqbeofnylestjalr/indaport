import React from 'react';
import Form from '../Form';

const PasswordRecovery = props => {
  return (
    <div className="form-wrap">
      <Form {...props} />
    </div>
  );
};

export default PasswordRecovery;
