import React from 'react';
import { Link } from 'react-router-dom';

import Form from '../Form';

const Login = props => {
  const { t } = props;

  return (
    <div className="form-wrap">
      <Form {...props} />
      <p>
        <Link to="/password-recovery">{t('password recovery')}!</Link>
      </p>
    </div>
  );
};

export default Login;
