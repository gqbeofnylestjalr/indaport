import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Form from '../Form';
import getQueryVars from '../../utils/getQueryVars';

class Confirmation extends React.Component {
  static propTypes = {
    t: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    editInput: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { location, editInput, form } = this.props;

    getQueryVars({
      querys: ['email', 'confirmation_hash'],
      location,
      editInput,
      formPath: form.formPath
    });
  }

  render() {
    const { t } = this.props;

    return (
      <div className="form-wrap">
        <Form {...this.props} />
        <p>
          {t('already have an account')}?<Link to="/login">{t('login')}!</Link>
        </p>
      </div>
    );
  }
}

export default Confirmation;
