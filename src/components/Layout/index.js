import React from 'react';

import Header from '../Header';
import Routes from '../../routes';
import Footer from '../Footer';

const Layout = props => {
  return props.ui.dataLoaded ? (
    <div className="page page-refill">
      {props.location.pathname !== '/' && <Header {...props} />}
      <Routes {...props} />
      <Footer {...props} />
    </div>
  ) : (
    <div className="page page-refill page-loader" />
  );
};
export default Layout;
