import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import InputGive from './InputGive';
import InputGet from './InputGet';
import InputAddress from './InputAddress';
import Links from './Links';
import ArrowBlue from '../Svg/ArrowBlue';

import {
  getRefillAddress,
  getExchangerAddress
} from '../../utils/refillHelpers';

class Refill extends Component {
  constructor() {
    super();

    this._isMounted = false;
    this.state = { status: 'pending' };
  }

  componentDidMount() {
    const { form } = this.props;

    this._isMounted = true;
    this.initMarkets();
    const market = this.getMarketFromQuery();
    this.getWalletAddress(market, form.get.currency);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  initMarkets = () => {
    const { user } = this.props;

    let markets = {};

    user.connectedApis.forEach(connectedApi => (markets[connectedApi] = {}));

    this.setState(markets);
  };

  getMarketFromQuery = () => {
    const { location, user, form, editInput } = this.props;

    const params = new URLSearchParams(location.search);
    const market = params.get('market');

    if (
      market &&
      market !== form.market.value &&
      user.connectedApis.findIndex(connectedApi => connectedApi === market) !==
        -1
    ) {
      editInput(form.formPath, 'market', market);
    }

    return market && market !== form.market.value ? market : form.market.value;
  };

  getWalletAddress = (market, currency) => {
    const { form, editInput } = this.props;

    if (this.state[market] && this.state[market][currency]) {
      const marketData = this.state[market][currency];

      editInput(form.formPath, 'walletAddress', { ...marketData });

      this.setState({ status: 'awaiting' });
    } else {
      getExchangerAddress({
        market,
        currency
      }).then(value => {
        const { address } = value;
        const tag = value.tag ? value.tag : '';

        editInput(form.formPath, 'walletAddress', { value: address, tag });

        if (this._isMounted) {
          this.setState({
            status: 'awaiting',
            [market]: {
              ...this.state[market],
              [currency]: { value: address, tag }
            }
          });
        }
      });
    }
  };

  changeCurrencyAndGetAddress = (formPath, field, { currency, tag }) => {
    const { form } = this.props;

    this.setState({ status: 'pending' });

    this.props.changeCurrency(formPath, field, { currency, tag });

    this.getWalletAddress(form.market.value, currency);
  };

  changeMarketAndGetAddress = (formPath, market) => {
    const { editInput, form } = this.props;

    this.setState({ status: 'pending' });

    editInput(formPath, 'market', market);

    this.getWalletAddress(market, form.get.currency);
  };

  render() {
    const {
      t,
      form,
      user,
      toggleDropdown,
      editInput,
      dropdowns,
      getValueFromPlaceholder,
      changeRefillInput,
      changeCurrency,
      currencies
    } = this.props;

    return (
      <div className="page-wrap">
        <div className="container">
          <h2>{t('quick deposit')}</h2>
          <p>
            {t('for work need')} <Link to="/connect-api">{t('connected')}</Link>
            , {t('at least')}
          </p>
          <div className="row justify-content-center align-items-center btns">
            <Links t={t} />
          </div>
          {user.connectedApis.length > 0 && (
            <Fragment>
              <div className="row justify-content-center align-items-center pt-5">
                <div className="col-12 col-md-6 col-lg">
                  <div className="refill-choose first">
                    <h6>{t('select exchange')}</h6>
                    <div
                      className={`refill-select ${
                        dropdowns.single === 'refillMomentGive'
                          ? 'refill-select_active'
                          : ''
                      }`}
                      onClick={() => {
                        toggleDropdown({
                          name: 'refillMarket',
                          mod: 'single'
                        });
                      }}>
                      <p>{form.market.value}</p>
                      <ArrowBlue />
                      <div
                        className={`refill-select-list ${
                          dropdowns.single === 'refillMarket'
                            ? 'refill-select-list_active'
                            : ''
                        }`}>
                        {user.connectedApis.map(connectedApi => (
                          <div
                            key={connectedApi}
                            onClick={() =>
                              this.changeMarketAndGetAddress(
                                form.formPath,
                                connectedApi
                              )
                            }
                            className="refill-select-list__item">
                            <p>{connectedApi}</p>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-12 col-md-6 col-lg">
                  <div className="refill-choose second">
                    <div className="row align-items-center">
                      <InputGive
                        t={t}
                        form={form}
                        currencies={currencies}
                        dropdowns={dropdowns}
                        toggleDropdown={toggleDropdown}
                        getValueFromPlaceholder={getValueFromPlaceholder}
                        changeRefillInput={changeRefillInput}
                        changeCurrency={changeCurrency}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-12 col-md-6 col-lg">
                  <div className="refill-choose second">
                    <div className="row align-items-center">
                      <InputGet
                        t={t}
                        form={form}
                        currencies={currencies}
                        dropdowns={dropdowns}
                        toggleDropdown={toggleDropdown}
                        changeRefillInput={changeRefillInput}
                        changeCurrency={this.changeCurrencyAndGetAddress}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-12 col-md-6 col-lg">
                  <InputAddress
                    t={t}
                    form={form}
                    editInput={editInput}
                    readonly
                  />
                </div>
              </div>
              <div className="row justify-content-center pt-5">
                <a
                  className="btn-refill active"
                  target="_blank"
                  rel="noopener noreferrer"
                  href={getRefillAddress(
                    user,
                    form,
                    this.state.status === 'awaiting' &&
                      (!form.walletAddress.tagRequired ||
                        form.walletAddress.tag.trim() !== '')
                  )}>
                  {t(form.i18n.submit)}
                </a>
              </div>
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

export default Refill;
