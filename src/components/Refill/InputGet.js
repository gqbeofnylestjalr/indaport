import React, { Component, Fragment } from 'react';

import ArrowBlue from '../Svg/ArrowBlue';
import DollarIcon from '../Svg/DollarIcon';

class InputGive extends Component {
  render() {
    const {
      t,
      form,
      currencies,
      dropdowns,
      changeRefillInput,
      toggleDropdown,
      changeCurrency
    } = this.props;

    return (
      <Fragment>
        <div className="col-6">
          <h6>{t('you get')}</h6>
          <input
            type="text"
            value={form.get.value}
            onChange={event =>
              changeRefillInput(event, form.formPath, 'get', 'give')
            }
          />
        </div>
        <div className="col-6">
          <div
            className={`refill-select ${
              dropdowns.single === 'refillMomentGet'
                ? 'refill-select_active'
                : ''
            }`}
            onClick={() =>
              toggleDropdown({
                name: `${form.formPath[0]}Get`,
                mod: 'single'
              })
            }>
            <img
              className="currency-icon"
              src={`https://indacoin.com/api/coinimage/${form.get.currency}/1`}
              alt={form.get.currency}
            />
            {form.get.currency}
            <ArrowBlue
              className={`icon-wrapper ${
                dropdowns.single === `${form.formPath[0]}Get`
                  ? 'icon-wrapper_up'
                  : ''
              }`}
            />
            <div
              className={`refill-select-list ${
                dropdowns.single === `${form.formPath[0]}Get`
                  ? 'refill-select-list_active'
                  : ''
              }`}>
              {currencies.crypto
                .filter(crypto => crypto.tag !== true)
                .slice(0, 5)
                .map(currency => (
                  <div
                    key={currency.short_name}
                    onClick={() =>
                      changeCurrency(form.formPath, 'get', {
                        currency: currency.short_name,
                        tag: currency.tag
                      })
                    }
                    className="refill-select-list__item">
                    <img
                      className="currency-icon"
                      src={`https://indacoin.com/api/coinimage/${
                        currency.short_name
                      }/1`}
                      alt={currency.short_name}
                    />
                    <p>{currency.short_name}</p>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default InputGive;
