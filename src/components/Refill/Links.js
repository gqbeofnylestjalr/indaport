import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';

const Links = ({ t }) => (
  <Fragment>
    <NavLink to="/refill" className="btn-refill" activeClassName="active">
      {t('deposit exchange')}
    </NavLink>
    <NavLink
      to="/refill-moment"
      className="btn-refill"
      activeClassName="active">
      {t('buy crypto')}
    </NavLink>
  </Fragment>
);

export default Links;
