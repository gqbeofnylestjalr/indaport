import React, { Component, Fragment } from 'react';

import ArrowBlue from '../Svg/ArrowBlue';
import DollarIcon from '../Svg/DollarIcon';

class InputGive extends Component {
  componentDidMount() {
    const { formPath, give } = this.props.form;

    if (give.value === '') {
      this.props.getValueFromPlaceholder(formPath);
    }
  }

  render() {
    const {
      t,
      form,
      currencies,
      dropdowns,
      toggleDropdown,
      changeRefillInput,
      changeCurrency
    } = this.props;

    const currencyFrom = currencies.real[form.give.currency];

    return (
      <Fragment>
        <div className="col-6">
          <h6>{t('you give')}</h6>
          <input
            type="text"
            onChange={event =>
              changeRefillInput(event, form.formPath, 'give', 'get')
            }
            value={form.give.value}
            placeholder={currencyFrom.fee.minSum}
          />
        </div>
        <div className="col-6">
          <div
            className={`refill-select ${
              dropdowns.single === 'refillMomentGive'
                ? 'refill-select_active'
                : ''
            }`}
            onClick={() => {
              toggleDropdown({
                name: `${form.formPath[0]}Give`,
                mod: 'single'
              });
            }}>
            <img
              className="currency-icon"
              src={`https://indacoin.com/api/coinimage/${form.give.currency}/1`}
              alt={form.give.currency}
            />
            {form.give.currency}
            <ArrowBlue
              className={`icon-wrapper ${
                dropdowns.single === `${form.formPath[0]}Give`
                  ? 'icon-wrapper_up'
                  : ''
              }`}
            />
            <div
              className={`refill-select-list ${
                dropdowns.single === `${form.formPath[0]}Give`
                  ? 'refill-select-list_active'
                  : ''
              }`}>
              {Object.values(currencies.real).map(currency => (
                <div
                  key={currency.factCurrencyId}
                  onClick={() =>
                    changeCurrency(form.formPath, 'give', {
                      currency: currency.factCurrencyId
                    })
                  }
                  className="refill-select-list__item">
                  <img
                    className="currency-icon"
                    src={`https://indacoin.com/api/coinimage/${
                      currency.factCurrencyId
                    }/1`}
                    alt={currency.factCurrencyId}
                  />
                  <p>{currency.factCurrencyId}</p>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default InputGive;
