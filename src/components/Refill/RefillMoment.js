import React from 'react';

import InputGive from './InputGive';
import InputGet from './InputGet';
import InputAddress from './InputAddress';
import Links from './Links';

import { getRefillAddress } from '../../utils/refillHelpers';

class RefillMoment extends React.Component {
  render() {
    const {
      t,
      form,
      user,
      editInput,
      toggleDropdown,
      dropdowns,
      getValueFromPlaceholder,
      changeRefillInput,
      changeCurrency,
      currencies
    } = this.props;

    return (
      <div className="page-wrap">
        <div className="container">
          <h2>{t('buy crypto momental')}</h2>
          <p>{t('indaport quick way for buy')}</p>
          <div className="row justify-content-center align-items-center btns">
            <Links t={t} />
          </div>
          <div className="row justify-content-center align-items-center pt-5">
            <div className="col-12 col-md-6 col-lg-3">
              <div className="refill-choose second">
                <div className="row align-items-center">
                  <InputGive
                    t={t}
                    form={form}
                    currencies={currencies}
                    dropdowns={dropdowns}
                    toggleDropdown={toggleDropdown}
                    getValueFromPlaceholder={getValueFromPlaceholder}
                    changeRefillInput={changeRefillInput}
                    changeCurrency={changeCurrency}
                  />
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div className="refill-choose second">
                <div className="row align-items-center">
                  <InputGet
                    t={t}
                    form={form}
                    currencies={currencies}
                    dropdowns={dropdowns}
                    changeRefillInput={changeRefillInput}
                    toggleDropdown={toggleDropdown}
                    changeCurrency={changeCurrency}
                  />
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-6">
              <InputAddress t={t} form={form} editInput={editInput} />
            </div>
          </div>
          <div className="row justify-content-center pt-5">
            <a
              className="btn-refill active"
              target="_blank"
              rel="noopener noreferrer"
              href={getRefillAddress(
                user,
                form,
                !form.walletAddress.tagRequired ||
                  form.walletAddress.tag.trim() !== ''
              )}>
              {t(form.i18n.submit)}
            </a>
          </div>
        </div>
      </div>
    );
  }
}
export default RefillMoment;
