import React, { Component } from 'react';

class InputAddress extends Component {
  render() {
    const { t, form, editInput, readonly = false } = this.props;

    return (
      <div className="refill-choose last">
        <h6>{t('cryptowallet address')}</h6>
        <div className="refill-select">
          <input
            type="text"
            readOnly={readonly}
            value={form.walletAddress.value || ''}
            onChange={event =>
              editInput(form.formPath, 'walletAddress', event.target.value)
            }
            placeholder="1P3shLkGqmB3cEp1LEwdpU8ghca5kNAoxh"
          />

          {form.walletAddress.tagRequired && (
            <input
              type="text"
              readOnly={readonly}
              className="refill-form-tag"
              value={form.walletAddress.tag}
              onChange={event =>
                editInput(form.formPath, 'walletAddress', {
                  tag: event.target.value
                })
              }
              placeholder={t('tag')}
            />
          )}
        </div>
      </div>
    );
  }
}

export default InputAddress;
