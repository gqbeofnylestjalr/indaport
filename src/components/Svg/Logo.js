import React from 'react';

const Logo = () => (
  <svg width="43" height="21" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clipPath="url(#clip0)">
      <path
        opacity=".28"
        d="M22.8 4.919l7.554 15.978h5.022L27.824 4.919H22.8zM7.742 4.954l7.554 15.978h5.022L12.766 4.954H7.742z"
        fill="#fff"
        stroke="#fff"
        strokeMiterlimit="10"
      />
      <path
        d="M27.785 4.919l-7.553 15.978h-5.024l7.554-15.978h5.023zM12.682 4.919L5.128 20.897H.108L7.66 4.919h5.022zM42.937 4.919l-7.553 15.978H30.36l7.554-15.978h5.023zM15.007.068l-1.55 3.24H8.431L9.983.068h5.024z"
        fill="#fff"
        stroke="#fff"
        strokeMiterlimit="10"
      />
    </g>
    <defs>
      <clipPath id="clip0">
        <path fill="#fff" d="M0 0h43v21H0z" />
      </clipPath>
    </defs>
  </svg>
);

export default Logo;
