import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Menu from './Menu';
import LogReg from './LogReg';
import Logo from '../Svg/Logo';
import ArrowBlack from '../Svg/ArrowBlack';
import Burger from './Burger';

class Header extends Component {
  render() {
    const { changeLanguage, t, toggleDropdown, user, logout } = this.props;

    const { dropdowns } = this.props.ui;

    return (
      <header className="header">
        <div className="container header__container">
          <div className="row header__row">
            <div className="col col-lg-2 header__col">
              <Link to="/" className="header__logo">
                <Logo className="header__logo-img" />
                <div className="header__logo-text">Indaport</div>
              </Link>
            </div>
            <Burger
              onClick={() =>
                toggleDropdown({ name: 'burger', mod: 'multiple' })
              }
              open={dropdowns.multiple.burger}
            />
            <div
              className={`header__navigation ${
                dropdowns.multiple.burger ? 'header__navigation_open' : ''
              }`}>
              {user.logined && <Menu t={t} />}
              <div className="log-reg">
                {!user.logined ? (
                  <LogReg t={t} />
                ) : (
                  <div className="header__user">
                    <div className="header__user-email">{user.email}</div>
                    <div className="btn-logout" onClick={() => logout()}>
                      {t('logout')}
                    </div>
                  </div>
                )}
              </div>
              <div className="header__options">
                <div className="header__lang-container">
                  <div
                    className="header__lang-change"
                    onClick={() =>
                      toggleDropdown({ name: 'langHeader', mod: 'multiple' })
                    }>
                    <p>{t('short name')}</p>
                    <div
                      className={`icon-wrapper${
                        dropdowns.multiple.langHeader ? ' icon-wrapper_up' : ''
                      }`}>
                      <ArrowBlack />
                    </div>
                  </div>
                  <div
                    className={`header__lang-list${
                      dropdowns.multiple.langHeader
                        ? ' header__lang-list_active'
                        : ''
                    }`}>
                    <div
                      onClick={() => {
                        changeLanguage('ru');
                        toggleDropdown({
                          name: 'langHeader',
                          mod: 'multiple'
                        });
                      }}
                      className="header__lang-list-item">
                      Русский
                    </div>
                    <div
                      onClick={() => {
                        changeLanguage('en');
                        toggleDropdown({
                          name: 'langHeader',
                          mod: 'multiple'
                        });
                      }}
                      className="header__lang-list-item">
                      English
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
