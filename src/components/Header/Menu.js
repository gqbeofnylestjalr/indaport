import React from 'react';
import { Link } from 'react-router-dom';

const Menu = ({ t }) => {
  return (
    <ul className="header__nav">
      <li className="header__nav-item">
        <Link className="header__nav-link" to="/connect-api">
          {t('birage')}
        </Link>
      </li>
      <li className="header__nav-item">
        <Link className="header__nav-link" to="/partners">
          {t('partners')}
        </Link>
      </li>
      <li className="header__nav-item">
        <Link className="header__nav-link" to="/settings">
          {t('settings')}
        </Link>
      </li>
      <li className="header__nav-item">
        <Link className="header__nav-link" to="/refill">
          {t('refill')}
        </Link>
      </li>
    </ul>
  );
};

export default Menu;
