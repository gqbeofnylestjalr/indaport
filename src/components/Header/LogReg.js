import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const LogReg = ({ t }) => {
  return (
    <Fragment>
      <Link to="/login">{t('login')}</Link>
      {' / '}
      <Link to="/registration">{t('registration')}</Link>
    </Fragment>
  );
};

export default LogReg;
