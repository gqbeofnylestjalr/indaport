import React from 'react';

import InvitedList from './InvitedList';
import PartnersProgram from './PartnersProgram';
import Socials from './Socials';

const Partners = ({ t }) => (
  <div className="page-partner page-wrap">
    <div className="container">
      <div className="row mt-5">
        <div className="col-12 col-md-6 mb-4">
          <PartnersProgram t={t} />
          <Socials t={t} />
        </div>
        <div className="col-12 col-md-6 mb-4">
          <InvitedList t={t} />
        </div>
      </div>
    </div>
  </div>
);

export default Partners;
