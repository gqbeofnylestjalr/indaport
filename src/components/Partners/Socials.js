import React from 'react';

import TelegramIcon from '../Svg/TelegramIcon';
import MailIcon from '../Svg/MailIcon';

const Socials = ({ t }) => (
  <div className="partner-block mt-4">
    <h3>{t('share in socials')}</h3>
    <div className="row soc m-0">
      <a href="#">
        <TelegramIcon />
      </a>
      <a href="#">
        <MailIcon />
      </a>
    </div>
  </div>
);

export default Socials;
