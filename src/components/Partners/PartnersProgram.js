import React from 'react';

const PartnersProgram = ({ t }) => (
  <div className="partner-block">
    <h3>{t('partners program')}</h3>
    <h6>{t('invite your friends')}</h6>
    <p>{t('partner link')}</p>
    <input
      className="form-control"
      type="text"
      value="https://www.indaport.com/ref_2001"
      readOnly
    />
    <p>{t('banner variants')}</p>
    <div className="table-wrap invite">
      <table className="table">
        <tbody>
          <tr>
            <td>
              <p>240х400</p>
            </td>
            <td>
              <p>
                <a href="#">{t('banner variants')}</a>
              </p>
            </td>
            <td>
              <p>
                <a href="#">{t('preview')}</a>
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>350х350</p>
            </td>
            <td>
              <p>
                <a href="#">{t('banner variants')}</a>
              </p>
            </td>
            <td>
              <p>
                <a href="#">{t('preview')}</a>
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>468х60</p>
            </td>
            <td>
              <p>
                <a href="#">{t('banner variants')}</a>
              </p>
            </td>
            <td>
              <p>
                <a href="#">{t('preview')}</a>
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>728х90</p>
            </td>
            <td>
              <p>
                <a href="#">{t('banner variants')}</a>
              </p>
            </td>
            <td>
              <p>
                <a href="#">{t('preview')}</a>
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div className="row align-items-center">
      <h4>{t('your code')}: 2001</h4>
      <button className="active">{t('save to buffer')}</button>
    </div>
  </div>
);

export default PartnersProgram;
