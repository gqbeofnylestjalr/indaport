import React from 'react';

const InvitedList = ({ t }) => (
  <div className="partner-block invited">
    <h3>{t('invited')} (21)</h3>
    <div className="table-wrap">
      <table className="table">
        <tbody>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
          <tr>
            <td>
              <p>23 июня 18:55</p>
            </td>
            <td>
              <p>aa@indacoin.com</p>
            </td>
            <td>
              <p>100 USDT</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div className="row justify-content-between align-items-center">
      <h4>{t('amount')}:</h4>
      <h4>1000 USDT</h4>
    </div>
  </div>
);

export default InvitedList;
