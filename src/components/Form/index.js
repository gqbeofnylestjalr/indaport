import React, { Fragment } from 'react';

const Form = props => {
  const { t, editInput, submitForm, form, imgSrc } = props;
  //TODO: refactor this shit

  const {
    i18n,
    formPath,
    fieldsList,
    status,
    method,
    valid,
    isSubmiting
  } = form;
  return (
    <form>
      {status === 'sending' || status === 'awaiting' ? (
        <Fragment>
          {fieldsList.map(fieldName => {
            const {
              Component,
              i18n,
              value,
              valid,
              type,
              readOnly,
              optional
            } = form[fieldName];

            return Component ? (
              <Component
                key={fieldName}
                t={t}
                imgSrc={imgSrc}
                text={t(form.i18n.title)}
              />
            ) : (
              <Fragment key={fieldName}>
                <label
                  className={`form-label ${
                    (value === '' && !isSubmiting) || (optional && value === '')
                      ? ''
                      : valid
                      ? 'form-label_valid'
                      : 'form-label_invalid'
                  }`}
                  htmlFor={fieldName}>
                  {t(i18n.label)}
                </label>
                <input
                  onChange={event =>
                    editInput(formPath, fieldName, event.target.value)
                  }
                  value={value}
                  name={fieldName}
                  id={fieldName}
                  type={type}
                  readOnly={readOnly}
                  className={`form-input ${
                    (value === '' && !isSubmiting) || (optional && value === '')
                      ? ''
                      : valid
                      ? 'form-input_valid'
                      : 'form-input_invalid'
                  }`}
                  placeholder={t(i18n.placeHolder)}
                />
              </Fragment>
            );
          })}
          <button
            className={`form-button ${
              status === 'sending' ? 'form-button_sending' : ''
            } ${
              valid === false && isSubmiting === true
                ? 'form-button_invalid'
                : ''
            }`}
            disabled={
              status === 'sending' || (valid === false && isSubmiting === true)
            }
            onClick={event =>
              submitForm({
                event,
                method,
                formPath: form.formPath
              })
            }>
            {t(status === 'sending' ? i18n.sending : i18n.submit)}
          </button>
        </Fragment>
      ) : (
        <div className="form-status">{t(status)}</div>
      )}
    </form>
  );
};

export default Form;
