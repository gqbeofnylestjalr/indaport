import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Form from '../Form';

class Registration extends Component {
  componentDidMount() {
    const { location, editInput, form } = this.props;
    const params = new URLSearchParams(location.search);

    const email = params.get('email');

    if (email) editInput(form.formPath, 'email', email);
  }

  render() {
    const { t } = this.props;

    return (
      <div className="form-wrap">
        <Form {...this.props} />
        <p>
          {t('already have an account')}? <Link to="/login">{t('login')}!</Link>
        </p>
      </div>
    );
  }
}

export default Registration;
