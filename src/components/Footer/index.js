import React from 'react';
import { Link } from 'react-router-dom';

import Logo from '../Svg/Logo';
import TwitterIcon from '../Svg/TwitterIcon';
import FacebookIcon from '../Svg/FacebookIcon';

const Footer = ({ t }) => (
  <footer className="footer">
    <div className="footer__container container">
      <div className="footer__row row">
        <div className="footer__col col-12 col-md-4">
          <Link to="/" className="footer__logo">
            <Logo /> Indaport
          </Link>
          <div className="footer__socials">
            <div className="footer__social-item">
              <FacebookIcon />
            </div>
            <div className="footer__social-item">
              <TwitterIcon />
            </div>
          </div>
        </div>
        <div className="footer__col col-6 col-md-2">
          <ul className="footer__navigation">
            <li className="footer__li footer__li_title">{t('birage')}</li>
            <li className="footer__li footer__li_link">{t('wallets')}</li>
            <li className="footer__li footer__li_link">{t('account')}</li>
            <li className="footer__li footer__li_link">{t('information')}</li>
          </ul>
        </div>
        <div className="footer__col col-6 col-md-2">
          <ul className="footer__navigation">
            <li className="footer__li footer__li_title">{t('buy')}</li>
            <li className="footer__li footer__li_link">{t('bitcoin')}</li>
            <li className="footer__li footer__li_link">{t('ethereum')}</li>
            <li className="footer__li footer__li_link">{t('ripple')}</li>
          </ul>
        </div>
        <div className="footer__col col-6 col-md-2">
          <ul className="footer__navigation">
            <li className="footer__li footer__li_title">{t('products')}</li>
            <li className="footer__li footer__li_link">{t('news')}</li>
            <li className="footer__li footer__li_link">{t('trading platform')}</li>
            <li className="footer__li footer__li_link">{t('mobile wallet')}</li>
          </ul>
        </div>
        <div className="footer__col col-6 col-md-2">
          <ul className="footer__navigation">
            <li className="footer__li footer__li_title">{t('our service')}</li>
            <li className="footer__li footer__li_link">{t('partnership')}</li>
            <li className="footer__li footer__li_link">{t('faq')}</li>
            <li className="footer__li footer__li_link">{t('terms of use')}</li>
          </ul>
        </div>
      </div>
    </div>
    <div className="footer__copyrights">{t('indacoin copyright')}</div>
  </footer>
);

export default Footer;
