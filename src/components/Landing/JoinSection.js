import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class JoinSection extends Component {
  constructor() {
    super();

    this.state = { email: '' };
  }

  onEmailChange = event => {
    this.setState({ email: event.target.value });
  };

  render() {
    const { t } = this.props;

    return (
      <section className="landing-section landing-section_join">
        <div className="container landing-section__container">
          <div className="row landing-section__row">
            <div className="col landing-section__col">
              <div className="landing-section__text landing-section__text_title">
                {t('join to indaport')}
              </div>
            </div>
          </div>
          <div className="row justify-content-center landing-section__row">
            <div className="col-12 col-md-8 landing-section__col">
              <div className="landing-join-form">
                <input
                  value={this.state.email}
                  onChange={this.onEmailChange}
                  className="landing-join-form__input"
                  placeholder={t('enter email')}
                />
                <Link
                  to={`/registration?email=${this.state.email}`}
                  className="landing-join-form__btn">
                  {t('registrate')}
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default JoinSection;
