import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Carousel } from 'antd';

import Header from '../Header';
import JoinSection from './JoinSection';

const NextArrow = props => {
  const { onClick } = props;

  return (
    <div onClick={onClick}>
      <img className="landing-feedback__arrow" alt="next" src="img/arrow.svg" />
    </div>
  );
};

const PrevArrow = props => {
  const { onClick } = props;

  return (
    <div onClick={onClick}>
      <img
        className="landing-feedback__arrow landing-feedback__arrow_prev"
        alt="prev"
        src="img/arrow.svg"
      />
    </div>
  );
};

const Landing = ({ t, ...rest }) => (
  <Fragment>
    <section className="landing-header">
      <Header t={t} {...rest} />
      <div className="container landing-header__container">
        <div className="row landing-header__row">
          <div className="col col-md-8 offset-md-2 landing-header__col ">
            <div className="landing-header__text landing-header__text_title">
              {t('innovative payment')}
            </div>
          </div>
        </div>
        <div className="row landing-header__row">
          <div className="col col-md-6 offset-md-3 landing-header__col ">
            <div className="landing-header__text landing-header__text_subtitle">
              {t('deposit you account')}
            </div>
          </div>
        </div>
        <div className="row landing-header__row">
          <div className="col landing-header__col landing-header__col_full ">
            <Link to="/registration" className="landing-header__btn">
              {t('begin')}
            </Link>
          </div>
        </div>
      </div>
    </section>
    <section className="landing-section">
      <div className="container landing-section__container">
        <div className="row landing-section__row">
          <div className="col landing-section__col">
            <div className="landing-section__text landing-section__text_title">
              {t('payment gateway')}
            </div>
          </div>
        </div>
        <div className="row justify-content-center landing-section__row">
          <div className="col-6 landing-section__col">
            <img
              src="img/laptop.png"
              alt="laptop"
              className="landing-section__img"
            />
          </div>
        </div>
      </div>
    </section>
    <section className="landing-section">
      <div className="container landing-section__container">
        <div className="row landing-section__row">
          <div className="col landing-section__col">
            <div className="landing-section__text landing-section__text_title">
              {t('exchanges connected to our service')}
            </div>
          </div>
        </div>
        <div className="row align-items-end justify-content-between landing-section__row">
          <div className="col-12 col-md-3 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/binance.png"
              alt="binance"
              className="landing-section__img landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-md-3 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/hitbtc.svg"
              alt="hitbtc"
              className="landing-section__img landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-md-3 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/bittrex.svg"
              alt="bittrex"
              className="landing-section__img landing-section__img_partner"
            />
          </div>
        </div>
      </div>
    </section>
    <section className="landing-section">
      <div className="container landing-section__container">
        <div className="row landing-section__row">
          <div className="col landing-section__col">
            <div className="landing-section__text landing-section__text_title">
              {t('begin right now')}
            </div>
          </div>
        </div>
        <div className="row justify-content-center landing-section__row">
          <div className="col-12 col-md-4 landing-section__col">
            <div className="landing-section__step">
              <img
                src="img/step1.png"
                alt="step1"
                className="landing-section__img landing-section__img_step landing-section__img_step-1"
              />
              <div className="landing-section__text landing-section__text_step">
                {t('create an account')}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-4 landing-section__col">
            <div className="landing-section__step">
              <img
                src="img/step2.png"
                alt="step2"
                className="landing-section__img landing-section__img_step landing-section__img_step-2"
              />
              <div className="landing-section__text landing-section__text_step">
                {t('connect birage')}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-4 landing-section__col">
            <div className="landing-section__step">
              <img
                src="img/step3.png"
                alt="step3"
                className="landing-section__img landing-section__img_step landing-section__img_step-3"
              />
              <div className="landing-section__text landing-section__text_step">
                {t('begin buying')}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="landing-section">
      <div className="container landing-section__container">
        <div className="row landing-section__row">
          <div className="col-12 col-md-3 landing-section__col">
            <div className="landing-section__text landing-section__text_stats-title landing-section__text_stats-white">
              {t('they trust us')}
            </div>
            <div className="landing-section__text landing-section__text_stats-subtitle">
              {t('24 hours stats')}
            </div>
          </div>
          <div className="col-12 col-md-3 landing-section__col">
            <div className="landing-section__stat">
              <div className="landing-section__text landing-section__text_stats-title">
                10 451
              </div>
              <div className="landing-section__text landing-section__text_stats-subtitle">
                {t('visits today')}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-3 landing-section__col">
            <div className="landing-section__stat">
              <div className="landing-section__text landing-section__text_stats-title">
                174 BTC
              </div>
              <div className="landing-section__text landing-section__text_stats-subtitle">
                {t('currency exchanged')}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-3 landing-section__col">
            <div className="landing-section__stat">
              <div className="landing-section__text landing-section__text_stats-title">
                1673
              </div>
              <div className="landing-section__text landing-section__text_stats-subtitle">
                {t('transactions complete')}
              </div>
            </div>
          </div>
        </div>
        <div className="row landing-section__row">
          <div className="col-12 landing-section__col">
            <Carousel
              arrows={true}
              dots={false}
              draggable={true}
              nextArrow={<NextArrow />}
              prevArrow={<PrevArrow />}>
              <div className="feedback-container">
                <div className="landing-feedback">
                  <div className="landing-feedback__photo-container">
                    <img
                      src="img/feedbackPhoto1.jpg"
                      alt="feedback"
                      className="landing-feedback__img landing-feedback__img_photo"
                    />
                  </div>
                  <div className="landing-feedback__content">
                    <div className="landing-feedback__text landing-feedback__text_title">
                      {t('feedback #1 name')}
                    </div>
                    <div className="landing-feedback__text  landing-feedback__text_subtitle">
                      {t('feedback #1 message')}
                    </div>
                    <div className="landing-feedback__meta">
                      <div className="landing-feedback__stars">
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star landing-feedback__img_star-unmarked"
                        />
                      </div>
                      <div className="landing-feedback__date">
                        <div className="landing-feedback__time">19:20</div>
                        <div className="landing-feedback__full-date">
                          29.12.2018
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="feedback-container">
                <div className="landing-feedback">
                  <div className="landing-feedback__photo-container">
                    <img
                      src="img/feedbackPhoto2.jpg"
                      alt="feedback"
                      className="landing-feedback__img landing-feedback__img_photo"
                    />
                  </div>
                  <div className="landing-feedback__content">
                    <div className="landing-feedback__text landing-feedback__text_title">
                      {t('feedback #2 name')}
                    </div>
                    <div className="landing-feedback__text  landing-feedback__text_subtitle">
                    {t('feedback #2 message')}
                    </div>
                    <div className="landing-feedback__meta">
                      <div className="landing-feedback__stars">
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                        <img
                          src="img/star.svg"
                          alt="star"
                          className="landing-feedback__img landing-feedback__img_star"
                        />
                      </div>
                      <div className="landing-feedback__date">
                        <div className="landing-feedback__time">11:18</div>
                        <div className="landing-feedback__full-date">
                          14.02.2019
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Carousel>
          </div>
        </div>
      </div>
    </section>
    <section className="landing-section">
      <div className="container landing-section__container">
        <div className="row landing-section__row">
          <div className="col landing-section__col">
            <div className="landing-section__text landing-section__text_title">
              {t('our partners')}
            </div>
          </div>
        </div>
        <div className="row align-items-center justify-content-center landing-section__row">
          <div className="col-12 col-sm-6 col-xl-4 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/partners/coinbase.png"
              alt="coinbase"
              className="landing-section__img landing-section__img_fix landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-sm-6 col-xl-4 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/partners/changelly.png"
              alt="changelly"
              className="landing-section__img landing-section__img_fix landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-sm-6 col-xl-4 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/partners/bitfinex.png"
              alt="bitfinex"
              className="landing-section__img landing-section__img_fix landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-sm-6 col-xl-4 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/partners/cryptopia.png"
              alt="cryptopia"
              className="landing-section__img landing-section__img_fix landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-sm-6 col-xl-4 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/partners/kucoin.png"
              alt="kucoin"
              className="landing-section__img landing-section__img_fix landing-section__img_partner"
            />
          </div>
          <div className="col-12 col-sm-6 col-xl-4 d-flex flex-wrap align-items-center justify-content-center landing-section__col landing-section__col_center">
            <img
              src="img/partners/shapeshift.png"
              alt="shapeshift"
              className="landing-section__img landing-section__img_fix landing-section__img_partner"
            />
          </div>
        </div>
      </div>
    </section>
    <JoinSection t={t} />
  </Fragment>
);

export default Landing;
