import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ConnectApi extends Component {
  render() {
    const { t, user } = this.props;
    return (
      <div className="page-deposit page-wrap">
        <div className="container">
          <h2>{t('select exchanges')}</h2>
          <div className="stock row align-items-center">
            <div className="col-12 col-lg">
              <img
                src="img/binance.png"
                alt="binance"
                style={{ width: 'auto', height: '35px' }}
              />
            </div>
            <div className="col-12 col-md-6 col-lg">
              <Link
                to={
                  user.connectedApis.findIndex(
                    connectedApi => connectedApi === 'binance'
                  ) !== -1
                    ? '/refill?market=binance'
                    : '#'
                }
                className={
                  user.connectedApis.findIndex(
                    connectedApi => connectedApi === 'binance'
                  ) !== -1
                    ? ''
                    : 'active'
                }>
                {t('deposit exchange')}
              </Link>
            </div>
            <div className="col-12 col-md-6 col-lg">
              <Link to="/connect-api/binance">
                {user.connectedApis.findIndex(
                  connectedApi => connectedApi === 'binance'
                ) !== -1
                  ? t('change api keys')
                  : t('connect api')}
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ConnectApi;
