import React from 'react';

import Form from '../Form';

const ConnectApiForm = props => {
  return (
    <div className="page-connect-api form-wrap">
      <Form {...props} />
    </div>
  );
};

export default ConnectApiForm;
