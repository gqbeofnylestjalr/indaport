const getQueryVars = ({ querys, editInput, formPath, location }) => {
  const params = new URLSearchParams(location.search);

  querys.forEach(query => {
    const value = params.get(query);

    if (value) editInput(formPath, query, value);
  });
};

export default getQueryVars;
