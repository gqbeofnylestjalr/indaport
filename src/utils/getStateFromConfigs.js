const getStatePartFromConfig = (config, path = []) => {
  let reduxStatePart = {};

  Object.entries(config).forEach(configPart => {
    const formName = configPart[0];
    const formValue = configPart[1];
    const { i18n, status, method, handler, isSubmiting } = formValue;

    reduxStatePart[formName] = {
      formPath: [...path, formName],
      i18n,
      status,
      method,
      isSubmiting,
      handler,
      fieldsList: []
    };

    if (formValue.fields) {
      formValue.fields.forEach(field => {
        reduxStatePart[formName].fieldsList.push(field.name);
        reduxStatePart[formName][field.name] = field.Component
          ? { ...field }
          : {
              value: '',
              ...field
            };
      });
    } else {
      reduxStatePart[formName] = getStatePartFromConfig(formValue, [
        ...path,
        formName
      ]);
    }
  });

  return reduxStatePart;
};

const getStateFromConfigs = configs => {
  let reduxState = {};

  Object.entries(configs).forEach(config => {
    const configName = config[0];
    const configValue = config[1];

    reduxState = {
      ...reduxState,
      ...getStatePartFromConfig({ [configName]: configValue })
    };
  });

  return reduxState;
};

export default getStateFromConfigs;
