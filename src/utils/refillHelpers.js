import axios from 'axios';

const getExchangerAmount = ({ amount, currencyFrom, currencyTo, mod = '' }) => {
  const { REACT_APP_IndacoinApiUrl, REACT_APP_ProjectName } = process.env;

  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${REACT_APP_IndacoinApiUrl}/GetCoinConvertAmount${mod}/${currencyFrom}/${currencyTo}/${amount}/${REACT_APP_ProjectName}`
    })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const getCryptoCurrencies = () => {
  const { REACT_APP_IndacoinApiUrl } = process.env;

  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${REACT_APP_IndacoinApiUrl}/uni/mobgetcurrenciesinfoi`
    })
      .then(response => {
        resolve(
          response.data.result.filter(currency => currency.isActive === true)
        );
      })
      .catch(error => {
        reject(error);
      });
  });
};

const getRealCurrencies = () => {
  const { REACT_APP_IndacoinApiUrl } = process.env;

  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${REACT_APP_IndacoinApiUrl}/getcashinfo/1`
    })
      .then(response => {
        const currencies = {};

        Object.values(response.data.cashTypes.cashIn)
          .filter(currency => {
            return currency.isCrypto === 0;
          })
          .forEach(currency => {
            currencies[currency.factCurrencyId] = currency;
          });

        resolve(currencies);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const getRefillAddress = (user, form, additionalCondition = true) => {
  const { REACT_APP_PARTNERNAME } = process.env;

  const email = user.email;
  const currencyFrom = form.give.currency;
  const currencyTo = form.get.currency;
  const amount = form.give.value;
  const address = form.walletAddress.value;
  const tag = form.walletAddress.tag;

  return form.valid && additionalCondition
    ? 'https://indacoin.com/gw/payment_form' +
        `?partner=${REACT_APP_PARTNERNAME}` +
        `&cur_from=${currencyFrom}` +
        `&cur_to=${currencyTo}` +
        `&amount=${amount}` +
        `&address=${address}` +
        `&user_id=${email}` +
        (tag ? `&tag=${tag}` : '')
    : null;
};

const checkAddress = ({ currency, address }) => {
  const { REACT_APP_IndacoinApiUrl } = process.env;

  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${REACT_APP_IndacoinApiUrl}/check_addr`,
      data: { currency, address }
    })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const getExchangerAddress = ({ market, currency }) => {
  const { REACT_APP_IndaportApiUrl } = process.env;

  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      withCredentials: true,
      url: `${REACT_APP_IndaportApiUrl}/getpartneruserdepositaddress`,
      data: { market, currency }
    })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

export {
  getExchangerAmount,
  getExchangerAddress,
  getRefillAddress,
  checkAddress,
  getCryptoCurrencies,
  getRealCurrencies
};
