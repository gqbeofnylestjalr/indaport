import React from 'react';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Layout from './components/Layout';

import i18n from './utils/i18n';

import { toggleDropdown, loadData } from './store/actions/ui';

import {
  editInput,
  editInputRaw,
  submitForm,
  getValueFromPlaceholder,
  changeRefillInput,
  changeCurrency
} from './store/actions/forms';

import { login, isLogined, logout } from './store/actions/user';

import { getCurrenciesLists } from './store/actions/currencies';

import './styles/main.sass';
import 'antd/dist/antd.css';

class App extends React.Component {
  componentDidMount() {
    this.props.loadData();
  }

  changeLanguage = lang => {
    if (i18n.language !== lang) {
      i18n.changeLanguage(lang);
    }
  };

  render() {
    return <Layout {...this.props} changeLanguage={this.changeLanguage} />;
  }
}

const mapStateToProps = state => ({ ...state });

export default withRouter(
  connect(
    mapStateToProps,
    {
      toggleDropdown,
      editInput,
      editInputRaw,
      submitForm,
      getValueFromPlaceholder,
      changeRefillInput,
      changeCurrency,
      login,
      logout,
      isLogined,
      getCurrenciesLists,
      loadData
    }
  )(withNamespaces()(App))
);
