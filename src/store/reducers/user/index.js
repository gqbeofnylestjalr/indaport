import initialState from './initialState';

function user(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN': {
      const { user } = action.payload;

      return { ...user, logined: true };
    }

    case 'LOGOUT': {
      return { ...state, email: '', logined: false };
    }

    default:
      return state;
  }
}

export default user;
