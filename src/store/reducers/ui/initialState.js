const initialState = {
  dropdowns: {
    multiple: {},
    single: ''
  },
  dataLoaded: false
};

export default initialState;
