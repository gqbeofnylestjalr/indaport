import initialState from './initialState';

import * as R from 'ramda';

function ui(state = initialState, action) {
  switch (action.type) {
    case 'TOGGLE_DROPDOWN': {
      const { name, mod } = action.payload;

      const lens =
        mod === 'single'
          ? R.lensPath(['dropdowns', mod])
          : R.lensPath(['dropdowns', mod, name]);
      const toggledValue =
        mod === 'single'
          ? state.dropdowns.single === name
            ? ''
            : name
          : !state.dropdowns[mod][name];

      return R.set(lens, toggledValue, state);
    }

    case 'SET_LOAD_STATUS': {
      const { status } = action.payload;

      return { ...state, dataLoaded: status };
    }

    default:
      return state;
  }
}

export default ui;
