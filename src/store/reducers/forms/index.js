import initialState from './initialState';

import * as R from 'ramda';

function forms(state = initialState, action) {
  switch (action.type) {
    case 'EDIT_INPUT': {
      const { value, formPath, name } = action.payload;

      const lens = R.lensPath([...formPath, name]);

      const oldField = R.view(lens, state);

      const updatedField =
        typeof value === 'object'
          ? { ...oldField, ...value }
          : { ...oldField, value };

      return R.set(lens, updatedField, state);
    }

    case 'SET_STATUS': {
      const { formPath, status } = action.payload;

      const lens = R.lensPath([...formPath, 'status']);

      return R.set(lens, status, state);
    }

    case 'VALIDATE': {
      const { formPath } = action.payload;

      const lens = R.lensPath([...formPath]);

      const form = R.view(lens, state);

      const validFields = form.fieldsList.filter(
        fieldName =>
          !!form[fieldName].Component ||
          form[fieldName].valid ||
          form[fieldName].optional === true
      );

      return validFields.length === form.fieldsList.length
        ? R.set(lens, { ...form, valid: true, isSubmiting: true }, state)
        : R.set(lens, { ...form, valid: false, isSubmiting: true }, state);
    }

    case 'CLEAR_FORM': {
      const { formPath } = action.payload;

      const lens = R.lensPath(formPath);
      const form = R.view(lens, state);
      let clearedForm = { ...form };

      form.fieldsList.forEach(fieldName => {
        clearedForm[fieldName] = {
          ...form[fieldName],
          value: form[fieldName].value !== undefined ? '' : undefined,
          valid: undefined
        };
      });
      clearedForm.valid = undefined;
      clearedForm.isSubmiting = false;
      clearedForm.status = 'awaiting';

      return R.set(lens, clearedForm, state);
    }

    case 'SET_INPUT_TIMER_ID': {
      const { formPath, id } = action.payload;

      const lens = R.lensPath([...formPath, 'inputTimerId']);

      return R.set(lens, id, state);
    }

    default:
      return state;
  }
}

export default forms;
