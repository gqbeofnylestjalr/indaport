import getStateFromConfigs from '../../../utils/getStateFromConfigs';
import formConfigs from '../../../configs/forms';

const initialState = {
  ...getStateFromConfigs(formConfigs)
};

export default initialState;
