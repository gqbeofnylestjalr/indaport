import * as R from 'ramda';

import initialState from './initialState';

function currencies(state = initialState, action) {
  switch (action.type) {
    case 'SET_CURRENCY_LIST': {
      const { type, list } = action.payload;

      const lens = R.lensPath([type]);

      return R.set(lens, list, state);
    }

    default:
      return state;
  }
}

export default currencies;
