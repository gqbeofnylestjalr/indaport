import axios from 'axios';

const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';

export function login(user) {
  return { type: LOGIN, payload: { user } };
}

export function isLogined() {
  return (dispatch, getState) => {
    const { REACT_APP_IndaportApiUrl } = process.env;

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        withCredentials: true,
        url: REACT_APP_IndaportApiUrl + '/islogined'
      })
        .then(response => {
          if (response.data.email !== '') {
            dispatch(login(response.data));
          }
          resolve();
        })
        .catch(error => {
          reject();
        });
    });
  };
}

export function logout() {
  return (dispatch, getState) => {
    const { REACT_APP_IndaportApiUrl } = process.env;

    axios({
      method: 'post',
      withCredentials: true,
      url: REACT_APP_IndaportApiUrl + '/logout'
    })
      .then(() => {
        dispatch({ type: LOGOUT });
      })
      .catch(error => {
        console.log(error);
      });
  };
}
