const SET_LOAD_STATUS = 'SET_LOAD_STATUS';

export function setLoadStatus(status) {
  return { type: SET_LOAD_STATUS, payload: { status } };
}
