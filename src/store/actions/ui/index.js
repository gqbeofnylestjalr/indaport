import { isLogined } from '../user';
import { getCurrenciesLists } from '../currencies';
import { setLoadStatus } from './internal';

const TOGGLE_DROPDOWN = 'TOGGLE_DROPDOWN';

export function toggleDropdown({ name, mod }) {
  return { type: TOGGLE_DROPDOWN, payload: { name, mod } };
}

export function loadData() {
  return (dispatch, getState) => {
    if (!getState().ui.dataLoaded) {
      dispatch(isLogined()).then(() => {
        const { currencies } = getState();

        Object.keys(currencies.real).length === 0 &&
        currencies.crypto.length === 0
          ? dispatch(getCurrenciesLists()).then(() => {
              dispatch(setLoadStatus(true));
            })
          : dispatch(setLoadStatus(true));
      });
    }
  };
}
