const SET_CURRENCY_LIST = 'SET_CURRENCY_LIST';

export function setCurrencyList(type, list) {
  return { type: SET_CURRENCY_LIST, payload: { type, list } };
}
