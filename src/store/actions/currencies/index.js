import { setCurrencyList } from './internal';

import {
  getCryptoCurrencies,
  getRealCurrencies
} from '../../../utils/refillHelpers';

export function getCurrenciesLists() {
  return dispatch => {
    return new Promise((resolve, reject) => {
      const promiseList = [getRealCurrencies(), getCryptoCurrencies()];

      Promise.all(promiseList)
        .then(lists => {
          dispatch(setCurrencyList('real', lists[0]));
          dispatch(setCurrencyList('crypto', lists[1]));
          resolve();
        })
        .catch(error => reject(error));
    });
  };
}
