const SET_STATUS = 'SET_STATUS';
const CLEAR_FORM = 'CLEAR_FORM';
const VALIDATE = 'VALIDATE';
const SET_INPUT_TIMER_ID = 'SET_INPUT_TIMER_ID';

export function validate(formPath) {
  return { type: VALIDATE, payload: { formPath } };
}

export function setStatus(formPath, status) {
  return { type: SET_STATUS, payload: { formPath, status } };
}

export function clearForm(formPath) {
  return { type: CLEAR_FORM, payload: { formPath } };
}

export function setInputTimerId(formPath, id) {
  return { type: SET_INPUT_TIMER_ID, payload: { formPath, id } };
}
