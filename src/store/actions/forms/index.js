import * as R from 'ramda';
import axios from 'axios';

import { validate, setStatus, clearForm, setInputTimerId } from './internal';
import { login, isLogined } from '../user';

import { getCurrenciesLists } from '../currencies';

import { setLoadStatus } from '../ui/internal';

import { getExchangerAmount } from '../../../utils/refillHelpers';

const EDIT_INPUT = 'EDIT_INPUT';

export function editInput(formPath, name, value) {
  return dispatch => {
    dispatch(editInputRaw(formPath, name, value));
    dispatch(validateField(formPath, name));
  };
}

export function editInputRaw(formPath, name, value) {
  return { type: EDIT_INPUT, payload: { formPath, name, value } };
}

export function validateField(formPath, field) {
  return (dispatch, getState) => {
    const { forms, currencies } = getState();

    const lens = R.lensPath(formPath);

    const form = R.view(lens, forms);

    const validationResult = form[field].validation({
      field,
      form,
      currency: currencies.real[form[field].currency]
    });

    if (Array.isArray(validationResult)) {
      validationResult.forEach(validationPiece => {
        dispatch(
          editInputRaw(formPath, validationPiece.field, validationPiece.result)
        );
      });
    } else {
      dispatch(editInputRaw(formPath, field, validationResult));
    }

    if (form.isSubmiting) {
      dispatch(validate(formPath));
    }
  };
}

export function submitForm({ event, method, formPath }) {
  event.preventDefault();

  return (dispatch, getState) => {
    dispatch(validate(formPath));

    const lens = R.lensPath(formPath);
    const form = R.view(lens, getState().forms);

    const { REACT_APP_IndaportApiUrl } = process.env;
    let data = {};

    form.fieldsList.forEach(fieldName => {
      data[fieldName] = form[fieldName].value;
    });

    if (form.valid) {
      dispatch(setStatus(formPath, 'sending'));
      axios({
        method: 'post',
        withCredentials: true,
        url: REACT_APP_IndaportApiUrl + method,
        data
      })
        .then(response => {
          form.handler({
            formPath,
            setStatus,
            dispatch,
            response,
            clearForm,
            login,
            getCurrenciesLists,
            setLoadStatus,
            isLogined,
            messageDuration: 2000
          });
        })
        .catch(error => {
          dispatch(setStatus(formPath, 'form error'));
          setTimeout(() => dispatch(setStatus(formPath, 'awaiting')), 1000);
        });
    }
  };
}

export function getValueFromPlaceholder(formPath) {
  return (dispatch, getState) => {
    const { currencies, forms } = getState();

    const lens = R.lensPath(formPath);

    const form = R.view(lens, forms);

    const currencyFrom = currencies.real[form.give.currency];

    getExchangerAmount({
      amount: currencyFrom.fee.minSum,
      currencyFrom: currencyFrom.factCurrencyId,
      currencyTo: form.get.currency
    }).then(currencyAmount => {
      dispatch(editInput(formPath, 'get', currencyAmount));
    });
  };
}

export function changeRefillInput(event, formPath, fieldFrom, fieldTo) {
  return (dispatch, getState) => {
    event.persist();
    const { value } = event.target;

    const lens = R.lensPath(formPath);

    const form = R.view(lens, getState().forms);

    const { inputTimerId } = form;

    dispatch(editInput(formPath, fieldFrom, value.trim()));

    const newTimerId = setTimeout(
      () => dispatch(recalculateValues(formPath, value, fieldTo)),
      150
    );

    clearTimeout(inputTimerId);

    dispatch(setInputTimerId(formPath, newTimerId));
  };
}

export function recalculateValues(formPath, value, fieldTo) {
  return (dispatch, getState) => {
    const lens = R.lensPath(formPath);

    const form = R.view(lens, getState().forms);

    if (typeof value === 'number' || (value.trim() !== '' && !isNaN(+value))) {
      getExchangerAmount({
        amount: value,
        currencyFrom: form.give.currency,
        mod: fieldTo === 'give' ? 'out' : '',
        currencyTo: form.get.currency
      }).then(amount => {
        dispatch(editInput(formPath, fieldTo, amount));
      });
    } else {
      if (value.trim() === '') {
        dispatch(getValueFromPlaceholder(formPath));
      } else {
        getExchangerAmount({
          amount: 0,
          currencyFrom: form.give.currency,
          currencyTo: form.get.currency
        }).then(amount => {
          dispatch(editInput(formPath, fieldTo, amount));
        });
      }
    }
  };
}

export function changeCurrency(formPath, field, value) {
  return (dispatch, getState) => {
    dispatch(
      editInput(formPath, field, {
        ...value
      })
    );

    const lens = R.lensPath(formPath);

    const form = R.view(lens, getState().forms);

    const { inputTimerId, walletAddress } = form;

    if (value.tag && !walletAddress.tagRequired) {
      dispatch(editInput(formPath, 'walletAddress', { tagRequired: true }));
    } else if (!value.tag && walletAddress.tagRequired) {
      dispatch(editInput(formPath, 'walletAddress', { tagRequired: false }));
    }

    const newTimerId = setTimeout(
      () => dispatch(recalculateValues(formPath, form.give.value, 'get')),
      150
    );

    clearTimeout(inputTimerId);

    dispatch(setInputTimerId(formPath, newTimerId));
  };
}
