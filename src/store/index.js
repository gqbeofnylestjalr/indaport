import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import ui from './reducers/ui';
import user from './reducers/user';
import forms from './reducers/forms';
import currencies from './reducers/currencies';

const rootReducer = combineReducers({ ui, user, forms, currencies });

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;
