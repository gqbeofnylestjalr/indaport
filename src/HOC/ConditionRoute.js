import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ConditionRoute = ({ condition, route = '/login', ...rest }) =>
  condition === true ? <Route {...rest} /> : <Redirect to={route} />;

export default ConditionRoute;
