# Indaport

![IMAGE](./doc/promo.gif)

Indaport is a cryptocurrency integration service. This site is written using React/Redux, Bootstrap & Sass

## Requirements

You will need the following things properly installed on your computer:

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with NPM)
* [create-react-app](https://facebook.github.io/create-react-app/)

## Installation

* `git clone <repository-url>`
* `cd indaport`
* `npm install`

In production, frontend server is serviced on node.js and express.js that is remotely located. After building, we get the assembled project folder "build". For daemonize node.js server we use PM2. Preparing the server:

* `npm install pm2@latest -g`
* `cd <frontend-server-path>`
* `npm install`
* `pm2 start server.js -i max`

## Running / Development

* `npm run start`
Visit your app at [http://localhost:3000](http://localhost:3000).

### Building

* `npm run build`

## Further Reading / Useful Links

* [React](https://reactjs.org/)
* [Redux](https://redux.js.org/)
* [React-router](https://reacttraining.com/react-router/web)
* [PM2](https://pm2.keymetrics.io/)
* Development Browser Extensions
  * [react developer tools for chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
  * [react developer tools for firefox](https://addons.mozilla.org/ru/firefox/addon/react-devtools/)
